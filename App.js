import React, { Component } from "react";
import {View, Text,StatusBar} from 'react-native'
import { Provider } from "react-redux";
import { Constants, Color} from "@common";
import configureStore from "./configurestore";
import { createAppContainer } from "react-navigation";
import {createStackNavigator} from "react-navigation-stack";

import LoginAction from './src/Actions/LoginAction';
import WelcomeAction from './src/Actions/WelcomeAction';
import SignupAction from './src/Actions/SignupAction';
import HomeAction from './src/Actions/HomeAction';
import ProfileAction from './src/Actions/ProfileAction';
import CourseAction from './src/Actions/CourseAction';
import ChooseAction from './src/Actions/ChooseAction';
import OTPAction from './src/Actions/OTPAction';


const Stack = createStackNavigator(
  {
    WelcomeScreen: { screen: WelcomeAction },
    LoginScreen:{ screen: LoginAction },
    SignupScreen:{ screen: SignupAction }, 
    HomeScreen: { screen: HomeAction },
    ProfileScreen : { screen: ProfileAction},
    CourseScreen : { screen: CourseAction},
    ChooseScreen : { screen: ChooseAction},
    OTPScreen : { screen : OTPAction},
    
  },
  {
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(Stack);
const store = configureStore();

class App extends Component {

  render() {
    console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"];
    StatusBar.setBackgroundColor(Color.statusBarColor)
      return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}

export default App;
