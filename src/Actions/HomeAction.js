import { HOME_FETCH } from '../Common/ConstantKey';
import HomeComponent from '../Components/HomeComponent';
import { connect } from 'react-redux';

export const DEFAULT_STATE_HOME = {
    isData: true,
}

export const Home_FetchData = (isData) => {
    return {
        type: HOME_FETCH,
        isData
    }
}

const mapStateToProps = (state) => {
    return {
        HomeData: state.HomeReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderFetch: () => {
            dispatch(Home_FetchData());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent);
