import { OTP_FETCH } from '../Common/ConstantKey';
import OTPComponent from '../Components/OTPComponent';
import { connect } from 'react-redux';

export const DEFAULT_STATE_OTP = {
    isData: true,
}

export const OTP_FetchData = (isData) => {
    return {
        type: OTP_FETCH,
        isData
    }
}

const mapStateToProps = (state) => {
    return {
        OTPData: state.OTPReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderFetch: () => {
            dispatch(OTP_FetchData());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(OTPComponent);
