import { LOGIN_FETCH } from '../Common/ConstantKey';
import LoginComponent from '../Components/LoginComponent';
import { connect } from 'react-redux';

export const DEFAULT_STATE_LOGIN = {
    isData: true,
}

export const Login_FetchData = (isData) => {
    return {
        type: LOGIN_FETCH,
        isData
    }
}

const mapStateToProps = (state) => {
    return {
        LoginData: state.LoginReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderFetch: () => {
            dispatch(Login_FetchData());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);
