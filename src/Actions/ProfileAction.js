import { PROFILE_FETCH } from '../Common/ConstantKey';
import ProfileComponent from '../Components/ProfileComponent';
import { connect } from 'react-redux';

export const DEFAULT_STATE_PROFILE = {
    isData: true,
}

export const Profile_FetchData = (isData) => {
    return {
        type: PROFILE_FETCH,
        isData
    }
}

const mapStateToProps = (state) => {
    return {
        ProfileData: state.ProfileReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderFetch: () => {
            dispatch(Profile_FetchData());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileComponent);
