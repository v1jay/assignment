import { WELCOME_FETCH } from '../Common/ConstantKey';
import WelcomeComponent from '../Components/WelcomeComponent';
import { connect } from 'react-redux';

export const DEFAULT_STATE_WELCOME = {
    isData: true,
}

export const Welcome_FetchData = (isData) => {
    return {
        type: WELCOME_FETCH,
        isData
    }
}

const mapStateToProps = (state) => {
    return {
        WelcomeData: state.WelcomeReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderFetch: () => {
            dispatch(Welcome_FetchData());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WelcomeComponent);
