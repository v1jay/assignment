import { COURSE_FETCH } from '../Common/ConstantKey';
import CourseComponent from '../Components/CourseComponent';
import { connect } from 'react-redux';

export const DEFAULT_STATE_COURSE = {
    isData: true,
}

export const Course_FetchData = (isData) => {
    return {
        type: COURSE_FETCH,
        isData
    }
}

const mapStateToProps = (state) => {
    return {
        CourseData: state.CourseReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderFetch: () => {
            dispatch(Course_FetchData());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CourseComponent);
