import { SIGNUP_FETCH } from '../Common/ConstantKey';
import SignupComponent from '../Components/SignupComponent';
import { connect } from 'react-redux';

export const DEFAULT_STATE_SIGNUP = {
    isData: true,
}

export const Signup_FetchData = (isData) => {
    return {
        type: SIGNUP_FETCH,
        isData
    }
}

const mapStateToProps = (state) => {
    return {
        SignupData: state.SignupReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderFetch: () => {
            dispatch(Signup_FetchData());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SignupComponent);
