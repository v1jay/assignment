import { CHOOSEE_FETCH } from '../Common/ConstantKey';
import ChooseComponent from '../Components/ChooseComponent';
import { connect } from 'react-redux';

export const DEFAULT_STATE_CHOOSE = {
    isData: true,
}

export const Choose_FetchData = (isData) => {
    return {
        type: CHOOSEE_FETCH,
        isData
    }
}

const mapStateToProps = (state) => {
    return {
        ChooseData: state.ChooseReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onRenderFetch: () => {
            dispatch(Choose_FetchData());
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ChooseComponent);
