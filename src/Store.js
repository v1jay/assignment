import { combineReducers, createStore } from 'redux'
import WelcomeReducer from './Reducers/WelcomeReducer';
import LoginReducer from './Reducers/LoginReducer';
import SignupReducer from './Reducers/SignupReducer';
import HomeReducer from './Reducers/HomeReducer';
import ProfileReducer from './Reducers/ProfileReducer';
import CourseReducer from './Reducers/CourseReducer';
import ChooseReducer from './Reducers/ChooseReducer';
import OTPReducer from './Reducers/OTPReducer';

const AppReducers = combineReducers({
    WelcomeReducer,LoginReducer,SignupReducer,HomeReducer,ProfileReducer,CourseReducer,ChooseReducer,OTPReducer
})

export default AppReducers;