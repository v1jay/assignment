import { SIGNUP_FETCH } from '../Common/ConstantKey';
import { DEFAULT_STATE_SIGNUP } from '../Actions/SignupAction';

const SignupReducer = (state = DEFAULT_STATE_SIGNUP, action) => {

    switch (action.type) {
        case SIGNUP_FETCH:
            return {
                ...state,
                isData: true,
            };

        default:
            return state;
    }

}

export default SignupReducer;