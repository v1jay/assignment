import { PROFILE_FETCH } from '../Common/ConstantKey';
import { DEFAULT_STATE_PROFILE } from '../Actions/ProfileAction';

const ProfileReducer = (state = DEFAULT_STATE_PROFILE, action) => {

    switch (action.type) {
        case PROFILE_FETCH:
            return {
                ...state,
                isData: true,
            };

        default:
            return state;
    }

}

export default ProfileReducer;