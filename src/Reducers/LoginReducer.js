import { LOGIN_FETCH } from '../Common/ConstantKey';
import { DEFAULT_STATE_LOGIN } from '../Actions/LoginAction';

const LoginReducer = (state = DEFAULT_STATE_LOGIN, action) => {

    switch (action.type) {
        case LOGIN_FETCH:
            return {
                ...state,
                isData: true,
            };

        default:
            return state;
    }

}

export default LoginReducer;