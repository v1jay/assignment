import { COURSE_FETCH } from '../Common/ConstantKey';
import { DEFAULT_STATE_COURSE } from '../Actions/CourseAction';

const CourseReducer = (state = DEFAULT_STATE_COURSE, action) => {

    switch (action.type) {
        case COURSE_FETCH:
            return {
                ...state,
                isData: true,
            };

        default:
            return state;
    }

}

export default CourseReducer;