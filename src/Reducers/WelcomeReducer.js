import { WELCOME_FETCH } from '../Common/ConstantKey';
import { DEFAULT_STATE_WELCOME } from '../Actions/WelcomeAction';

const WelcomeReducer = (state = DEFAULT_STATE_WELCOME, action) => {

    switch (action.type) {
        case WELCOME_FETCH:
            return {
                ...state,
                isData: true,
            };

        default:
            return state;
    }

}

export default WelcomeReducer;