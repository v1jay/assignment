import { OTP_FETCH } from '../Common/ConstantKey';
import { DEFAULT_STATE_OTP } from '../Actions/OTPAction';

const OTPReducer = (state = DEFAULT_STATE_OTP, action) => {

    switch (action.type) {
        case OTP_FETCH:
            return {
                ...state,
                isData: true,
            };

        default:
            return state;
    }

}

export default OTPReducer;