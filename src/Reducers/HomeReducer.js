import { HOME_FETCH } from '../Common/ConstantKey';
import { DEFAULT_STATE_HOME } from '../Actions/HomeAction';

const HomeReducer = (state = DEFAULT_STATE_HOME, action) => {

    switch (action.type) {
        case HOME_FETCH:
            return {
                ...state,
                isData: true,
            };

        default:
            return state;
    }

}

export default HomeReducer;