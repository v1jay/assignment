import { CHOOSEE_FETCH } from '../Common/ConstantKey';
import { DEFAULT_STATE_CHOOSE } from '../Actions/ChooseAction';

const ChooseReducer = (state = DEFAULT_STATE_CHOOSE, action) => {

    switch (action.type) {
        case CHOOSEE_FETCH:
            return {
                ...state,
                isData: true,
            };

        default:
            return state;
    }

}

export default ChooseReducer;