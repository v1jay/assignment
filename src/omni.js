import _Icon from "react-native-vector-icons/MaterialCommunityIcons";
export const Icon = _Icon;

import _IconIO from "react-native-vector-icons/Ionicons";
export const IconIO = _IconIO;

import { PixelRatio } from "react-native";