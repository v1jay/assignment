import { StyleSheet } from 'react-native'
import { Constants, Color} from "@common";

export default StyleSheet.create({

    TextStyle: {
        fontSize: 15,alignSelf:'center',color: Color.primaryDark
},
ButtonView:{
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
    marginBottom: 10
},
ButtonStyle:{
      height : 45,
      backgroundColor: "#ff1f5b",
      borderRadius: 5,
      marginRight: 5, 
      width: '100%',
      justifyContent:'center'
},
TxtInptstyle: {marginLeft: 10,marginRight: 10,height: 40,fontSize: 15,marginTop: 2},
Iconstyle: {marginTop:8,marginLeft: 8},
subView: {width: '80%', flexDirection: 'row',marginTop: -4},
LineView: {marginBottom: 10, borderColor: Color.primaryDark, borderWidth: 0.5 },
SignupTxt: { fontSize: 20, color: Color.White, fontWeight:'bold'},
AccntTxt: {marginLeft: 15, color:Color.primaryDark,fontSize: 15},  
TermTxt:{marginRight: 15, color:Color.primaryDark,fontSize: 15},
ViewStyle: {marginLeft:20,marginRight:20,flexDirection:'row',justifyContent:'space-between'},
MainView: { marginTop: 30, marginBottom: 10, marginLeft: 30, marginRight: 30, height: 200, borderColor: Color.primaryDark, borderWidth: 1, borderRadius: 5 },
})