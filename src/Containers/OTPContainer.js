import { StyleSheet } from 'react-native'
import { Constants, Color} from "@common";

export default StyleSheet.create({

    TextStyle: {
        fontSize: 15,alignSelf:'center',color: Color.primaryDark
},
ButtonView:{
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
    marginBottom: 10
},
ButtonStyle:{
      height : 45,
      backgroundColor: "#ff1f5b",
      borderRadius: 5,
      marginRight: 5, 
      width: '100%',
      justifyContent:'center'
},
LineView:{ marginTop: 3, marginBottom: 10, borderColor: Color.primaryDark, borderWidth: 0.5 },
SignupTxt: { fontSize: 20, color: Color.White, fontWeight:'bold'},
AccntTxt: {marginLeft: 15, color:Color.primaryDark,fontSize: 15},  
TermTxt:{marginRight: 15, fontSize: 16,fontWeight:'bold'},
ViewStyle: {marginLeft:20,marginRight:20,flexDirection:'row',justifyContent:'space-between',marginTop: 10},
LoginView: { height: 80, justifyContent: 'center' },
LoginTxt:{ fontSize: 30, fontWeight: 'bold', alignSelf: 'center' },
WelView: { height: 130, justifyContent: 'center', backgroundColor: '#ececec' },
WelTxt: { fontSize: 20, fontWeight: 'bold', alignSelf: 'center' },
MainView: { margin: 20, justifyContent: 'center' ,flexDirection:'row',alignItems:'center'},
BoxView: {height: 50,width: 50,borderColor: "#cfcfcf",borderWidth: 1,borderRadius: 5,alignContent:'center'},
BoxTxt: {marginLeft: 15,fontSize: 18},
OTPView: {flexDirection: 'row',justifyContent:'space-between',alignContent:'center',marginTop:10,marginLeft: 40,marginRight:40,marginBottom: 10},
})