import { StyleSheet } from 'react-native'
import { Constants, Color} from "@common";

export default StyleSheet.create({

    bannerContainer: {
        flex: 1,
        // height:'80%',
        justifyContent: 'center',
        marginBottom: 10
      },
      ButtonView:{
          margin: 20
      },
      ButtonStyle:{
            height : 45,
            backgroundColor: "#ff1f5b",
            borderRadius: 5,
            marginRight: 5, 
            width: '100%',
            justifyContent:'center'
      },
      LoginTxt: {marginRight: 25, color:"#ffc42b",fontWeight:'bold',fontSize: 16},
      AccntTxt: {marginLeft: 20, color:Color.primaryDark,fontSize: 15},  
      ViewStyle: {marginBottom: 20,flexDirection:'row',justifyContent:'space-between'},
      SignupTxt: { fontSize: 20, color: Color.White, fontWeight:'bold'},
})