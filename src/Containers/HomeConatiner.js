import { StyleSheet } from 'react-native'
import { Constants, Color} from "@common";

export default StyleSheet.create({
ViewRect: { marginTop: 40, marginBottom: 50, marginLeft: 30, marginRight: 30, height: 100, borderColor: Color.primaryDark, borderWidth: 1, borderRadius: 5 },
mainView:{ marginTop: 5, width: '100%', flexDirection: 'row', justifyContent: 'space-between',marginBottom: 5 },
LineView:{ marginTop: 5, marginBottom: 10, borderColor: Color.primaryDark, borderWidth: 0.5 },
HdrTxt: { alignSelf: 'flex-end', fontSize: 18 },
welView: { height: 40, backgroundColor: '#0b1132', flexDirection: 'row' },
welTxt: { fontSize: 18, color: Color.primaryDark, marginLeft: 15 },
AccView1: { flexDirection: 'row', justifyContent: 'space-between', marginLeft: 45, marginRight: 45 },
AccView2: { flexDirection: 'row', justifyContent: 'space-between', marginLeft: 45, marginRight: 45, marginTop: 20, marginBottom: 30 },
MainView2: { backgroundColor: Color.White, flex: 1, borderRadius: 30 },
SubView2: { flexDirection: 'row', justifyContent: 'space-between', marginLeft: 45, marginRight: 45, marginTop: 20 },
SubViewTxt1: { marginTop: 8, width: 60, fontSize: 15, color: Color.Black, fontWeight: 'bold' },
SubViewTxt2: { marginTop: 8, width: 60, fontSize: 15, color: Color.Black, fontWeight: 'bold', marginLeft: 8 },
EliteTXT: { fontSize: 20, fontWeight: 'bold', marginLeft: 15, marginTop: 40 },
ImgView: { borderRadius: 10, width: 200, height: 200, },
FtrTxt: { fontSize: 12 ,color:Color.primaryDark},
ImgTxtView: { borderRadius: 10, backgroundColor: Color.White, width: 110, height: 30, justifyContent: 'center', marginLeft: 15, marginTop: 120 },
})