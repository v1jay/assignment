import { StyleSheet } from 'react-native'
import { Constants, Color} from "@common";

export default StyleSheet.create({

    TextStyle: {
        fontSize: 15,alignSelf:'center',color: Color.primaryDark
},
ButtonView:{
    marginLeft: 30,
    marginRight: 30,
    marginTop: 10,
    marginBottom: 10
},
ButtonStyle:{
      height : 45,
      backgroundColor: "#ff1f5b",
      borderRadius: 5,
      marginRight: 5, 
      width: '100%',
      justifyContent:'center'
},
LineView:{ marginTop: 3, marginBottom: 10, borderColor: Color.primaryDark, borderWidth: 0.5 },
OTPTxt:{ marginRight: 15, color: "#ffc42b", fontWeight: 'bold', fontSize: 16, alignContent: 'flex-end',marginTop: 8 },
subView:{ flexDirection: 'row',width:'60%', },
mainView:{ marginTop: 5, width: '100%', flexDirection: 'row', justifyContent: 'space-between' },
SignupTxt: { fontSize: 20, color: Color.White, fontWeight:'bold'},
AccntTxt: {marginLeft: 15, color:Color.primaryDark,fontSize: 15},  
TermTxt:{marginRight: 15, color:Color.primaryDark,fontSize: 15},
ViewStyle: {marginLeft:20,marginRight:20,flexDirection:'row',justifyContent:'space-between'},
LoginView: { height: 80, justifyContent: 'center' },
LoginTxt:{ fontSize: 30, fontWeight: 'bold', alignSelf: 'center' },
WelView: { height: 130, justifyContent: 'center', backgroundColor: '#ececec' },
WelTxt: { fontSize: 20, fontWeight: 'bold', alignSelf: 'center' },
ViewRect: { marginTop: 10, marginBottom: 10, marginLeft: 30, marginRight: 30, height: 100, borderColor: Color.primaryDark, borderWidth: 1, borderRadius: 5 },
})