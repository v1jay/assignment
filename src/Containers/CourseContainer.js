import { StyleSheet } from 'react-native'
import { Constants, Color} from "@common";

export default StyleSheet.create({

SubView1: { flexDirection: 'row', justifyContent: 'space-between', marginLeft: 40, marginRight: 40, marginTop: 40 },
LoginView: { height: 80, justifyContent: 'center' },
LoginTxt:{ fontSize: 30, fontWeight: 'bold', alignSelf: 'center' },
IconTxt: {marginTop: 8,fontSize: 15, color: Color.Black},
ExmTxt: {marginTop: 10,marginLeft: 20,fontSize: 15,color: Color.primaryDark,fontWeight:'bold'},
})