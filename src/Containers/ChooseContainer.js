import { StyleSheet } from 'react-native'
import { Constants, Color} from "@common";

export default StyleSheet.create({
    HdrTxt: {fontSize: 16 ,color: Color.Black},
SubView1: { flexDirection: 'row', justifyContent: 'space-between', marginLeft: 40, marginRight: 40, marginTop: 40 },
LoginView: { height: 80, justifyContent: 'center' },
LoginTxt:{ fontSize: 30, fontWeight: 'bold', alignSelf: 'center' },
})