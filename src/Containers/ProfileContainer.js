import { StyleSheet } from 'react-native'
import { Constants, Color} from "@common";

export default StyleSheet.create({
    HdrTxt: { alignSelf: 'flex-end', fontSize: 18 },
    welView: { height: 350, backgroundColor: '#0b1132', flexDirection: 'row',borderRadius: 30,marginTop: -30 },
PerformanceTxt: { fontSize: 15, color: Color.primaryDark, marginLeft:5,marginTop: 30 },
MainView2: { backgroundColor: Color.White, flex: 1, borderRadius: 30 },
SubView1: { flexDirection: 'row', justifyContent: 'space-between', marginLeft: 30, marginRight: 30, marginTop: 30 },
SubView2: { flexDirection: 'row', justifyContent: 'space-between', marginLeft: 30, marginRight: 45, marginTop: 30 },
SubViewTxt1: { marginTop: 8, width: 90, fontSize: 15, color: Color.Black, fontWeight: 'bold' },
MainView:{flexDirection: 'row', justifyContent: 'space-between', marginTop: 20},
})