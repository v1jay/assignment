import React, { Component } from 'react';
import { Text, View, StatusBar, FlatList, ImageBackground } from 'react-native';
import { Container, Button, Header, Right, Left, Body, Footer, FooterTab } from 'native-base';
import styles from '../Containers/ChooseContainer';
import { Constants, Color, Icons } from "@common";
import { Icon } from '@app/omni';
import { ScrollView, TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';


const Data = [
    {
        Icn: Icons.MaterialCommunityIcons.time,
        textData: 'UGC NET HRM'
    },
    {
        Icn: Icons.MaterialCommunityIcons.flower,
        textData: 'UGC NET MANAGEMENT'

    },
    {
        Icn: Icons.MaterialCommunityIcons.stack,
        textData: 'UGC NET COMMERCE'

    },
    {
        Icn: Icons.MaterialCommunityIcons.clipboard,
        textData: 'UGC NET IAW'

    },
    {
        Icn: Icons.MaterialCommunityIcons.cap,
        textData: 'UGC NET ECONOMICS'

    },
    {
        Icn: Icons.MaterialCommunityIcons.test,
        textData: 'UGC NET GENERAL PAPER'

    }

];

export default class ChooseComponent extends Component {
    constructor(Props) {
        super(Props)
        this.state = {
        };
    }

    componentDidMount() {
        console.error = (error) => error.apply;
        this.props.onRenderFetch();
        console.disableYellowBox = true;
    }

    render() {
        if (this.props.ChooseData.isData) {
            StatusBar.setBackgroundColor(Color.statusBarColor)
            return (
                <Container style={{ backgroundColor: Color.White }}>
                    <Header style={{ backgroundColor: Color.White, height: 50 }} noShadow>
                        <Left>
                            <Button transparent
                              onPress= {()=>{this.props.navigation.navigate('CourseScreen')}}>
                                <Icon name={Icons.MaterialCommunityIcons.Back} size={25} style={{ color: Color.Black }} />
                            </Button>
                        </Left>
                        <Body><Text style={styles.HdrTxt}>Choose Course</Text></Body>
                        <Right></Right>
                    </Header>
                    <ScrollView>
                        <View style={{ marginTop: 40, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ marginLeft: 20, fontSize: 15, color: Color.primaryDark, fontWeight: 'bold' }}>Exam Preparation</Text>
                            <View style={{ marginRight: 20, borderRadius: 5, height: 30, width: '30%', backgroundColor: '#ffe8e8', justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 15, color: 'red', fontWeight: 'bold' }}>UGC NET</Text>
                            </View>
                        </View>

                        <View>

                            <FlatList
                                data={Data}
                                keyExtractor={(item, index) => index}
                                renderItem={({ item }) =>

                 <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, marginLeft: 30 }}>
                                <Icon name={item.Icn}
                                           onPress= {()=>{this.props.navigation.navigate('HomeScreen')}}
                               style = {{color:Color.primaryDark}}
                                    onPress={() => { this.props.navigation.navigate('ChooseScreen') }} size={25} />
                                <Text style={{ marginLeft: 20, fontWeight: 'bold', color: Color.primaryDark }}  onPress= {()=>{this.props.navigation.navigate('HomeScreen')}}>{item.textData}</Text>
                            </View>
        
                                }
                            />

                        </View>
                    </ScrollView>

                </Container>
            )
        }
    }
}