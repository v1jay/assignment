import React, { Component } from 'react';
import { Text, View, StatusBar, Image, ImageBackground, FlatList } from 'react-native';
import { Container, Button, Header, Right, Left, Body, Title, Footer, FooterTab } from 'native-base';
import styles from '../Containers/HomeConatiner';
import { Constants, Color, Icons } from "@common";
import { Icon } from '@app/omni';
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native';
import { ScrollView } from 'react-native-gesture-handler';



const Data = [
    {
        img: require('../Images/ic_img1.jpg'),
        textData: 'Exam Notification'
    },
    {
        img: require('../Images/ic_img2.jpg'),
        textData: 'Books'

    },
    {
        img: require('../Images/ic_img3.jpg'),
        textData: 'Exam Result'

    }

];




export default class HomeComponent extends Component {
    constructor(Props) {
        super(Props)
        this.state = {
        };
    }

    componentDidMount() {
        console.error = (error) => error.apply;
        this.props.onRenderFetch();
        console.disableYellowBox = true;
    }

    render() {
        if (this.props.HomeData.isData) {
            StatusBar.setBackgroundColor(Color.statusBarColor)
            return (
                <Container style={{ backgroundColor: Color.bgColor }}>
                    <Header style={{ backgroundColor: '#0b1132', height: 70 }}>
                        <Left></Left>
                        <Body><Title style={styles.HdrTxt}>Home</Title></Body>
                        <Right>
                            <Button transparent style={{ marginRight: -20 }}>
                                <Icon name={Icons.MaterialCommunityIcons.Search} size={25} style={{ color: Color.White }} />
                            </Button>
                            <Button transparent>
                                <Icon name={Icons.MaterialCommunityIcons.bell} size={25} style={{ color: Color.White }} />
                            </Button>
                        </Right>
                    </Header>
                        <View style={styles.welView}>
                            <Text style={styles.welTxt}>Welcome Back,</Text>
                            <Text style={{ fontSize: 18, color: Color.White, marginLeft: 5 }}>Rahul!</Text>
                        </View>
                        <ScrollView>
                        <View>
                            <Collapse style={{ backgroundColor: Color.bgColor }}>
                                <CollapseHeader style={{ backgroundColor: Color.bgColor }}>
                                    <Header style={{ backgroundColor: Color.bgColor }} noShadow>
                                        <Left style={{ flex: 1, marginLeft: 10 }}>
                                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Subjects</Text>
                                        </Left>
                                        <Right>
                                            <Icon name={Icons.MaterialCommunityIcons.Down} size={35} style={{ marginRight: 5 }} />
                                        </Right>
                                    </Header>
                                </CollapseHeader>
                                <CollapseBody>

                                    <View style={styles.AccView1}>
                                        <View>
                                            <Image
                                                source={require('../Images/ic_bk1.png')} />
                                            <Text>Subject 01</Text>
                                        </View>

                                        <View>
                                            <Image
                                                source={require('../Images/ic_bk3.png')} />
                                            <Text>Subject 02</Text>
                                        </View>

                                        <View>
                                            <Image
                                                source={require('../Images/ic_bk2.png')} />
                                            <Text>Subject 03</Text>
                                        </View>
                                    </View>

                                    <View style={styles.AccView2}>
                                        <View>
                                            <Image
                                                source={require('../Images/ic_bk1.png')} />
                                            <Text>Subject 04</Text>
                                        </View>

                                        <View>
                                            <Image
                                                source={require('../Images/ic_bk3.png')} />
                                            <Text>Subject 05</Text>
                                        </View>

                                        <View>
                                            <Image
                                                source={require('../Images/ic_bk2.png')} />
                                            <Text>Subject 06</Text>
                                        </View>
                                    </View>


                                </CollapseBody>
                            </Collapse>
                        </View>

                        <View style={styles.MainView2}>

                            <View style={styles.SubView2}>
                                <View>
                                    <Image
                                        source={require('../Images/ic_sub1.png')} />
                                    <Text style={styles.SubViewTxt1}>Learning Analysis</Text>
                                </View>

                                <View>
                                    <Image
                                        source={require('../Images/ic_sub3.png')} />
                                    <Text style={styles.SubViewTxt2}>Quiz</Text>
                                </View>

                                <View>
                                    <Image
                                        source={require('../Images/ic_sub2.png')} />
                                    <Text style={styles.SubViewTxt1}>   Free Feature</Text>
                                </View>
                            </View>

                            <Text style={styles.EliteTXT}>Elite Corner</Text>
                            <Text style={{ fontSize: 15, color: Color.primaryDark, marginLeft: 15, marginTop: 8 }}>Lorem ipsum dolor sit amet, consectet</Text>


                            <View>
                                <FlatList
                                    data={Data}
                                    keyExtractor={(item, index) => index}
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={({ item }) =>
                                        <View style={{ marginTop: 25, marginLeft: 30 }}>
                                            <View style={styles.ImgView}>
                                                <ImageBackground source={item.img} style={{ width: '100%', height: '100%', borderRadius: 10, }}>
                                                    <View style={styles.ImgTxtView}>
                                                        <Text style={{ fontSize: 12, color: Color.Black, marginLeft: 5 }}>{item.textData}</Text>

                                                    </View>
                                                    <View style={{ width: 150, justifyContent: 'center', marginLeft: 15, marginTop: 5 }}>
                                                        <Text style={{ color: Color.White, fontWeight: 'bold' }}>Lorem ipsum dolor sit amet, consectet</Text>
                                                    </View>
                                                </ImageBackground>
                                            </View>
                                        </View>
                                    }
                                />
                            </View>

                            <View style={styles.ViewRect}>
                                <View style={styles.mainView}>
                                    <Text style={{ marginLeft: 15, fontSize: 18, marginTop: 8 }}>Recently learned</Text>
                                </View>
                                <View style={styles.LineView} />
                                <View style={{ width: '100%' }}>
                                    <Text style={{ marginLeft: 15, fontSize: 18, fontWeight: 'bold', marginTop: 3 }}>Resume</Text>
                                </View>
                            </View>
                        </View>
                    </ScrollView>

                    <Footer style={{ backgroundColor: Color.White }}>
                        <FooterTab style={{ backgroundColor: Color.White }}>
                            <Button vertical>
                                <Icon name={Icons.MaterialCommunityIcons.Home} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={styles.FtrTxt}>Home</Text>
                            </Button>
                            <Button vertical>
                                <Icon name={Icons.MaterialCommunityIcons.bookmark} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={styles.FtrTxt}>Bookmarks</Text>
                            </Button>
                            <Button vertical>
                                <Icon name={Icons.MaterialCommunityIcons.camera} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={styles.FtrTxt}>Live</Text>
                            </Button>
                            <Button vertical>
                                <Icon name={Icons.MaterialCommunityIcons.test} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={styles.FtrTxt}>Test</Text>
                            </Button>
                            <Button vertical
                            onPress= {()=>{this.props.navigation.navigate('ProfileScreen')}}>
                                <Icon name={Icons.MaterialCommunityIcons.Account} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={styles.FtrTxt}>More</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            )
        }
    }
}