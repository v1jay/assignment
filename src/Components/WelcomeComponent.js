import React, { Component } from 'react';
import { Text, View, StatusBar,Image } from 'react-native';
import { Container, Button, Header, Right,Icon } from 'native-base';
import styles from '../Containers/WelcomeContainer';
import { Constants, Color} from "@common";
import Carousel from 'react-native-banner-carousel'

const images = [
    {
        img: require('../Images/ic_bnr1.png')
    },
    {
        img: require('../Images/ic_bnr2.png')
    },
    {
        img: require('../Images/ic_bnr3.png')
    }

];


export default class WelcomeComponent extends Component {
    componentDidMount() {
        console.error = (error) => error.apply;
        this.props.onRenderFetch();
        console.disableYellowBox = true;
    }
    

    constructor() {
        super();
    }


    renderPage(image, index) {
        return (
            <View>
                <Image style={{ width: Constants.BannerWidth,height: 800,resizeMode:"cover"}} source={image.img} />
            </View>
        );
    }
navigateto(){
    this.props.navigation.navigate('LoginScreen');
}

    render() {
        if (this.props.WelcomeData.isData) {
            StatusBar.setBackgroundColor(Color.statusBarColor)
            return (
                <Container>
                <View style={styles.bannerContainer}>
                <Carousel autoplay={true}
                  autoplayTimeout={5000}
                  activePageIndicatorStyle={{ backgroundColor: Color.primary }}
                  loop={true}
                  pageSize={Constants.BannerWidth}>
                           {images.map((image, index) => this.renderPage(image, index))}
                </Carousel>
                   
              </View>
              <View style = {styles.ButtonView}>
              <Button style={styles.ButtonStyle}
               onPress= {()=>{this.props.navigation.navigate('SignupScreen')}}>
                  <Text style ={styles.SignupTxt}>SIGNUP</Text>
              </Button>
               </View>
               <View style = {styles.ViewStyle}>

<Text style = {styles.AccntTxt}>Already have an account?</Text>

    <Text style = {styles.LoginTxt} onPress= {()=>this.navigateto()}>Login</Text>

               </View>

                </Container>
            )
        }
    }
}