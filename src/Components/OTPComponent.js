import React, { Component } from 'react';
import { Text, View, StatusBar, TextInput } from 'react-native';
import { Container, Button, Header, Right, Icon } from 'native-base';
import styles from '../Containers/OTPContainer';
import { Constants, Color } from "@common";



export default class OTPComponent extends Component {
constructor(Props){
super(Props)
this.state={
    text1:'',
    text2:'',
    text3:'',
    text4:'',

};
}

    componentDidMount() {
        console.error = (error) => error.apply;
        this.props.onRenderFetch();
        console.disableYellowBox = true;
    }

    render() {
        if (this.props.OTPData.isData) {
        StatusBar.setBackgroundColor(Color.statusBarColor)
        return (
            <Container>
                <View style={styles.LoginView}>
                    <Text style={styles.LoginTxt}>Login</Text>
                </View>
                <View style={styles.WelView} />
                <View style={styles.MainView}>
<Text style = {styles.AccntTxt}>Enter the OTP sent to</Text>
<Text style={{fontSize: 16,fontWeight:'bold',marginLeft: 7,marginTop: -2}}>+91-987654321</Text>
</View>

<View style = {styles.OTPView}>

    <View style={styles.BoxView}>
        <TextInput
                          style={styles.BoxTxt}
                          placeholder="7"
                          keyboardType= {'numeric'}
                          maxLength= {1}
                          returnKeyType = {"next"}
                          onChangeText={(text1) => this.setState({text1})}
                          value={this.state.text1}
        />
    </View>

    <View style={styles.BoxView}>
        <TextInput
                          style={styles.BoxTxt}
                          placeholder="7"
                          keyboardType= {'numeric'}
                          maxLength= {1}
                          returnKeyType = {"next"}
                          onChangeText={(text2) => this.setState({text2})}
                          value={this.state.text2}
        />
    </View>

    <View style={styles.BoxView}>
        <TextInput
                          style={styles.BoxTxt}
                          placeholder="7"
                          keyboardType= {'numeric'}
                          maxLength= {1}
                          returnKeyType = {"next"}
                          onChangeText={(text3) => this.setState({text3})}
                          value={this.state.text3}
        />
    </View>

    <View style={styles.BoxView}>
        <TextInput
                          style={styles.BoxTxt}
                          placeholder="7"
                          keyboardType= {'numeric'}
                          maxLength= {1}   
                          returnKeyType = {"done"}
                          onChangeText={(text4) => this.setState({text4})}
                          value={this.state.text4}
        />
    </View>


</View>

                <View style = {styles.ButtonView}>
              <Button style={styles.ButtonStyle}
              onPress= {()=>{this.props.navigation.navigate('CourseScreen')}}>
                  <Text style ={styles.SignupTxt}>VERIFY & PROCEED</Text>
              </Button>
               </View>

               <View style = {styles.ViewStyle}>

<Text style = {styles.AccntTxt}>Don't receive the OTP?</Text>

    <Text style = {styles.TermTxt}>Resend OTP</Text>
               </View>

            </Container>
        )
    }
}
}