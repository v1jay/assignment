import React, { Component } from 'react';
import { Text, View, StatusBar, TextInput } from 'react-native';
import { Container, Button, Header, Right, Icon } from 'native-base';
import styles from '../Containers/LoginContainer';
import { Constants, Color } from "@common";



export default class LoginComponent extends Component {
constructor(Props){
super(Props)
this.state={
    text1:'',
    text2:''

};
}

    componentDidMount() {
        console.error = (error) => error.apply;
        this.props.onRenderFetch();
        console.disableYellowBox = true;
    }

    render() {
        if (this.props.LoginData.isData) {
        StatusBar.setBackgroundColor(Color.statusBarColor)
        return (
            <Container>
                <View style={styles.LoginView}>
                    <Text style={styles.LoginTxt}>Login</Text>
                </View>
                <View style={styles.WelView}>
                    <Text style={styles.WelTxt}>Welcome Back!</Text>
                    <Text style={styles.TextStyle}>We will really miss you.</Text>
                </View>
                <View style={{ marginRight: 20, marginLeft: 20, justifyContent: 'center' }}>
                    <Text style={styles.TextStyle}>Enter your Registered Mobile Number</Text>
                    <Text style={styles.TextStyle}>we will send a 4 Digit Number</Text>
                </View>

                <View style={styles.ViewRect}>
                    <View style={styles.mainView}>
                        <View style={styles.subView}>
                            <Text style={{ marginLeft: 15, fontSize: 18, fontWeight: 'bold',marginTop: 8 }}>+91</Text>
                            <TextInput
                            style={{marginLeft: 10,marginRight: 10,height: 40,fontSize: 15,marginTop: 2}}
          placeholder="Contact Number"
          keyboardType= {'numeric'}
          onChangeText={(text1) => this.setState({text1})}
          value={this.state.text1}
        />
                        </View>
                        <View>
                            <Text style={styles.OTPTxt}>Send OTP</Text>
                        </View>
                    </View>
                    <View style={styles.LineView} />
                    <View style={{width: '100%'}}>
                    <TextInput
                            style={{marginLeft: 10,marginRight: 10,height: 40,fontSize: 15,marginTop:-5}}
          placeholder="Enter OTP"
          keyboardType= {'numeric'}
          onChangeText={(text2) => this.setState({text2})}
          value={this.state.text2}
        />
                    </View>
                </View>

                <View style = {styles.ButtonView}>
              <Button style={styles.ButtonStyle}
              onPress= {()=>{this.props.navigation.navigate('CourseScreen')}}>
                  <Text style ={styles.SignupTxt}>LOGIN</Text>
              </Button>
               </View>

               <View style = {styles.ViewStyle}>
<View style={{flexDirection:'row'}}>
<Text style = {styles.AccntTxt}>Don't have an account?</Text>
<Text style={{fontSize: 16,fontWeight:'bold',textDecorationLine:'underline',marginLeft: 5,marginTop: -2}}  onPress= {()=>{this.props.navigation.navigate('SignupScreen')}}>Create</Text>
</View>
    <Text style = {styles.TermTxt}>Terms</Text>
               </View>

            </Container>
        )
    }
}
}