import React, { Component } from 'react';
import { Text, View, StatusBar, Image, ImageBackground } from 'react-native';
import { Container, Button, Header, Right, Left, Body, Title, Footer, FooterTab } from 'native-base';
import styles from '../Containers/CourseContainer';
import { Constants, Color, Icons } from "@common";
import { Icon } from '@app/omni';
import { ScrollView } from 'react-native-gesture-handler';


export default class CourseComponent extends Component {
    constructor(Props) {
        super(Props)
        this.state = {
        };
    }

    componentDidMount() {
        console.error = (error) => error.apply;
        this.props.onRenderFetch();
        console.disableYellowBox = true;
    }

    render() {
        if (this.props.CourseData.isData) {
            StatusBar.setBackgroundColor(Color.statusBarColor)
            return (
                <Container style={{ backgroundColor: Color.White }}>
                   <View style={styles.LoginView}>
                    <Text style={styles.LoginTxt}>Choose Course</Text>
                </View>
                    <ScrollView>

<Text style = {styles.ExmTxt}>Exam Preparation</Text>
  <View style={styles.SubView1}>
                                    <View style={{alignItems:"center"}} >
                                <Icon name={Icons.MaterialCommunityIcons.test}
                                 onPress= {()=>{this.props.navigation.navigate('ChooseScreen')}}   size={35} />
                                    <Text style={styles.IconTxt}>UGC NET</Text>
                                    </View>

                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.cap} onPress= {()=>{this.props.navigation.navigate('ChooseScreen')}}   size={35} />
                                    <Text style={styles.IconTxt}>CSIR NET</Text>
                                </View>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.clipboard} onPress= {()=>{this.props.navigation.navigate('ChooseScreen')}}   size={35} />
                                    <Text style={styles.IconTxt}>IIT JAM</Text>
                                </View>
                        </View>

                        <View style={styles.SubView1}>
                                    <View style={{alignItems:"center"}} >
                                <Icon name={Icons.MaterialCommunityIcons.test}
                                 onPress= {()=>{this.props.navigation.navigate('ChooseScreen')}}   size={35} />
                                    <Text style={styles.IconTxt}>UGC NET</Text>
                                    </View>

                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.cap} onPress= {()=>{this.props.navigation.navigate('ChooseScreen')}}   size={35} />
                                    <Text style={styles.IconTxt}>CSIR NET</Text>
                                </View>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.clipboard} onPress= {()=>{this.props.navigation.navigate('ChooseScreen')}}   size={35} />
                                    <Text style={styles.IconTxt}>IIT JAM</Text>
                                </View>
                        </View>
                        
                        <View style={styles.SubView1}>
                                    <View style={{alignItems:"center"}} >
                                <Icon name={Icons.MaterialCommunityIcons.test}
                                 onPress= {()=>{this.props.navigation.navigate('ChooseScreen')}}   size={35} />
                                    <Text style={styles.IconTxt}>UGC NET</Text>
                                    </View>

                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.cap} onPress= {()=>{this.props.navigation.navigate('ChooseScreen')}}   size={35} />
                                    <Text style={styles.IconTxt}>CSIR NET</Text>
                                </View>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.clipboard} onPress= {()=>{this.props.navigation.navigate('ChooseScreen')}}   size={35} />
                                    <Text style={styles.IconTxt}>IIT JAM</Text>
                                </View>
                        </View>



                    </ScrollView>

                </Container>
            )
        }
    }
}