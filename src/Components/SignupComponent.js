import React, { Component } from 'react';
import { Text, View, StatusBar, TextInput } from 'react-native';
import { Container, Button, Header, Right } from 'native-base';
import styles from '../Containers/SignupContainer';
import { Constants, Color,Icons } from "@common";
import { Icon } from '@app/omni';


export default class SignupComponent extends Component {
constructor(Props){
super(Props)
this.state={
    text1:'',
    text2:'',
    text3: '',
    text4:'',

};
}

    componentDidMount() {
        console.error = (error) => error.apply;
        this.props.onRenderFetch();
        console.disableYellowBox = true;
    }

    render() {
        if (this.props.SignupData.isData) {
        StatusBar.setBackgroundColor(Color.statusBarColor)
        return (
            <Container>
                <View style={{ height: 80, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 24, fontWeight: 'bold', alignSelf: 'center' }}>Create Account</Text>
                </View>
                <View style={styles.MainView}>
                    <View style={{ marginTop: 5, width: '80%', flexDirection: 'row'}}>
                            <Icon name= {Icons.MaterialCommunityIcons.Account}
                            size={25}
                            style = {styles.Iconstyle} />
                            <TextInput
                            style={styles.TxtInptstyle}
          placeholder="Name"
          onChangeText={(text1) => this.setState({text1})}
          value={this.state.text1}
        />
                    </View>
                    <View style={styles.LineView} />
                   

                    <View style={styles.subView}>
                            <Icon name= {Icons.MaterialCommunityIcons.phone}
                            size={25}
                            style = {styles.Iconstyle} />
                            <TextInput
                            style={styles.TxtInptstyle}
          placeholder="Mobile Number"
          keyboardType={'numeric'}
          onChangeText={(text2) => this.setState({text2})}
          value={this.state.text2}
        />
                    </View>
                    <View style={styles.LineView} />


                    <View style={styles.subView}>
                            <Icon name= {Icons.MaterialCommunityIcons.mail}
                            size={25}
                            style = {styles.Iconstyle} />
                            <TextInput
                            style={styles.TxtInptstyle}
          placeholder="Email ID"
          onChangeText={(text3) => this.setState({text3})}
          value={this.state.text3}
        />
                    </View>
                    <View style={styles.LineView} />

                   
                    <View style={styles.subView}>
                            <Icon name= {Icons.MaterialCommunityIcons.city}
                            size={25}
                            style = {styles.Iconstyle} />
                            <TextInput
                            style={styles.TxtInptstyle}
          placeholder="City / Nearest Location"
          onChangeText={(text4) => this.setState({text4})}
          value={this.state.text}
        />
                    </View>

                </View>

                <View style = {styles.ButtonView}>
              <Button style={styles.ButtonStyle}
              onPress= {()=>{this.props.navigation.navigate('OTPScreen')}}
              >
                  <Text style ={styles.SignupTxt}>REGISTER</Text>
              </Button>
               </View>

               <View style = {styles.ViewStyle}>
<View style={{flexDirection:'row'}}>
<Text style = {styles.AccntTxt}>Already have an account?</Text>
</View>
<Text style={{fontSize: 16,fontWeight:'bold',marginRight: 15}}  onPress= {()=>{this.props.navigation.navigate('LoginScreen')}}>Login</Text>    
           </View>

            </Container>
        )
    }
}
}