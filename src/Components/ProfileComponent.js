import React, { Component } from 'react';
import { Text, View, StatusBar, Image, ImageBackground } from 'react-native';
import { Container, Button, Header, Right, Left, Body, Title, Footer, FooterTab } from 'native-base';
import styles from '../Containers/ProfileContainer';
import { Constants, Color, Icons } from "@common";
import { Icon } from '@app/omni';
import { ScrollView } from 'react-native-gesture-handler';


export default class ProfileComponent extends Component {
    constructor(Props) {
        super(Props)
        this.state = {
        };
    }

    componentDidMount() {
        console.error = (error) => error.apply;
        this.props.onRenderFetch();
        console.disableYellowBox = true;
    }

    render() {
        if (this.props.ProfileData.isData) {
            StatusBar.setBackgroundColor(Color.statusBarColor)
            return (
                <Container style={{ backgroundColor: Color.bgColor }}>
                    <Header style={{ backgroundColor: '#0b1132', height: 70 }}>
                        <Left></Left>
                        <Body><Title style={styles.HdrTxt}>Profile</Title></Body>
                        <Right>
                            <Button transparent style={{ marginRight: -20 }}>
                                <Icon name={Icons.MaterialCommunityIcons.Search} size={25} style={{ color: Color.White }} />
                            </Button>
                            <Button transparent>
                                <Icon name={Icons.MaterialCommunityIcons.bell} size={25} style={{ color: Color.White }} />
                            </Button>
                        </Right>
                    </Header>
                    <ScrollView>

                        <View style={styles.welView}>

<View style = {{flexDirection: 'row', justifyContent: 'space-between', marginTop: 20,width:'100%'}}>
<View style={{marginTop: 50,alignItems:'center'}}>
<Text style={styles.PerformanceTxt}>Perfomace</Text>
<Text style={{ color:"#ffc42b",fontWeight:'bold',fontSize: 18}}>30%</Text>
</View>
<View style ={{
    flex: 1,
marginTop: 20,
    alignItems: 'center',
    marginBottom: -25,
    marginLeft: '25%',
    paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0}}>
<Image source={require('../Images/ic_avatar.png')}
              style={{width: 150, height: 150, borderRadius: 150/2}} />
              <View style = {{alignItems:'center',marginLeft: 10,marginRight:10,marginTop: 10,width: Constants.BannerWidth}}>
<Text style={{ color: Color.White,fontWeight:'bold',fontSize: 18}}>Rahul Krishnan</Text>
<Text style={{ontSize: 15, color: Color.primaryDark,marginTop: 5}}>Beginner</Text>
<View style = {{marginTop: 30,flexDirection: 'row', justifyContent: 'space-evenly',width: Constants.BannerWidth}}>

<View style = {{alignItems:'center'}}>
<Icon name={Icons.MaterialCommunityIcons.Account} size={35} style = {{color:Color.White}} />
<Text style={{ fontSize: 14 ,color:Color.White}}>Profile</Text>
</View>

<View style = {{alignItems:'center'}}>
<Icon name={Icons.MaterialCommunityIcons.badge} size={35} style = {{color:Color.White}} />
<Text style={{ fontSize: 14 ,color:Color.White}}>Badges</Text>
</View>

<View style = {{alignItems:'center'}}>
<Icon name={Icons.MaterialCommunityIcons.quiz} size={35} style = {{color:Color.White}} />
<Text style={{ fontSize: 14 ,color:Color.White}}>Quiz</Text>
</View>

<View style = {{alignItems:'center'}}>
<Icon name={Icons.MaterialCommunityIcons.book} size={35} style = {{color:Color.White}} />
<Text style={{ fontSize: 14 ,color:Color.White}}>Books</Text>
</View>

</View>
</View>
</View>
<View style={{marginTop: 50,alignItems:'center',marginLeft:100,marginRight: 10}}>
<Text style={{ fontSize: 15, color: Color.primaryDark,marginTop: 30 }}>Score</Text>
<View style = {{flexDirection: 'row'}}>
<Text style={{ color: Color.White,fontWeight:'bold',fontSize: 18}}>80</Text>
<Text style={{ color: 'red',fontWeight:'bold',fontSize: 18}}>/100</Text>
</View>
</View>
</View>
 </View>

  <View style={styles.SubView1}>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.bookmark} size={25} />
                                    <Text style={{marginTop: 8,fontSize: 15, color: Color.Black}}>Bookmarks</Text>
                                </View>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.bell} size={25} />
                                    <Text style={{marginTop: 8,fontSize: 15, color: Color.Black}}>Notifications</Text>
                                </View>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.buynow} size={25} />
                                    <Text style={{marginTop: 8,fontSize: 15, color: Color.Black}}>Buy now</Text>
                                </View>
                        </View>

                        <View style={styles.SubView2}>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.contact} size={25} />
                                    <Text style={{marginTop: 8,fontSize: 15, color: Color.Black}}>Contact us</Text>
                                </View>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.myorder} size={25} />
                                    <Text style={{marginTop: 8,fontSize: 15, color: Color.Black}}>My Orders</Text>
                                </View>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.Help} size={25} />
                                    <Text style={{marginTop: 8,fontSize: 15, color: Color.Black}}>Help</Text>
                                </View>
                        </View>


                        <View style={styles.SubView1}>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.voucher} size={25} />
                                    <Text style={{marginTop: 8,fontSize: 15, color: Color.Black}}>Redeem</Text>
                                    <Text style={{fontSize: 15, color: Color.Black}}>Voucher</Text>
                                </View>
                                <View style={{alignItems:"center",marginLeft: 20}}>
                                <Icon name={Icons.MaterialCommunityIcons.share} size={25} />
                                    <Text style={{marginTop: 8,fontSize: 15, color: Color.Black}}>Share the</Text>
                                    <Text style={{fontSize: 15, color: Color.Black}}>App</Text>
                                </View>
                                <View style={{alignItems:"center"}}>
                                <Icon name={Icons.MaterialCommunityIcons.person} size={25} />
                                    <Text style={{marginTop: 8,fontSize: 15, color: Color.Black,}}>Placement</Text>
                                    <Text style={{fontSize: 15, color: Color.Black,}}>Assistance</Text>
                                </View>
                        </View>


                    </ScrollView>

                    <Footer style={{ backgroundColor: Color.White }}>
                    <FooterTab style={{ backgroundColor: Color.White }}>
                            <Button vertical
                            onPress= {()=>{this.props.navigation.navigate('HomeScreen')}}>
                                <Icon name={Icons.MaterialCommunityIcons.Home} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={{ fontSize: 12 ,color:Color.primaryDark}}>Home</Text>
                            </Button>
                            <Button vertical>
                                <Icon name={Icons.MaterialCommunityIcons.bookmark} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={{ fontSize: 12 ,color:Color.primaryDark}}>Bookmarks</Text>
                            </Button>
                            <Button vertical>
                                <Icon name={Icons.MaterialCommunityIcons.camera} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={{ fontSize: 12 ,color:Color.primaryDark}}>Live</Text>
                            </Button>
                            <Button vertical>
                                <Icon name={Icons.MaterialCommunityIcons.test} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={{ fontSize: 12 ,color:Color.primaryDark}}>Test</Text>
                            </Button>
                            <Button vertical>
                                <Icon name={Icons.MaterialCommunityIcons.Account} size={25} style = {{color:Color.primaryDark}} />
                                <Text style={{ fontSize: 12 ,color:Color.primaryDark}}>More</Text>
                            </Button>
                        </FooterTab>
                    </Footer>
                </Container>
            )
        }
    }
}