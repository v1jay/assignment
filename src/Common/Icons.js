//Check out at 'http://ionicframework.com/docs/v2/ionicons/' for more icons
// 'react-native-vector-icons/Ionicons'
const Ionicons = {
    Menu: "ios-menu",
    Home: "ios-home",
    SignIn: "ios-log-in",
    SignOut: "ios-log-out",
    Delete: "ios-trash",
    Add: "ios-add-outline",
    Back: "ios-arrow-back",
    Search: "ios-search",
    Lock: "md-lock",
  };
  
  // 'react-native-vector-icons/MaterialCommunityIcons'
  const MaterialCommunityIcons = {
    Menu: "menu",
    Home: "home",
    buynow: "cart-arrow-down",
    SignOut: "logout",
    Delete: "delete",
    Add: "plus",
    Edit: "pencil",
    sort: "sort-variant",
    empty: "delete-empty",
    Help: "help-circle-outline",
    Back: "arrow-left",
    Search: "magnify",
    Account: "account",
    bell: "bell-outline",
    Reply: "reply",
    phone: "cellphone-iphone",
    mail: "email-outline",
    city: "city-variant-outline",
    Down: "chevron-down",
    bookmark: "bookmark-minus-outline",
    camera: "camera-metering-center",
    test: "playlist-edit",
quiz:"puzzle-outline",
badge:"seal",
book:"library-books",
contact:"phone",
myorder:"van-utility",
voucher: "credit-card",
share: "share-variant",
person: "account-group-outline",
cap: "school",
clipboard: "clipboard-text-outline",
time: "timelapse",
flower: "flower",
stack: "chart-bar-stacked",


  };  
  
  export default { MaterialCommunityIcons, Ionicons };