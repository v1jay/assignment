export default {
	statusBarColor: '#0b1132',
	primaryDark: '#696a6d',
	primaryLight: '#B2EBF2',
	primary: '#ff6f61',
	secondary: '#ff6f61',
	toastText: '#fff',
	Black: '#000000',
	bgColor: '#e3e3e3',
	White: '#ffffff'

};