export const WELCOME_FETCH = "WELCOME_FETCH";
export const OTP_FETCH = "OTP_FETCH";
export const LOGIN_FETCH = "LOGIN_FETCH";
export const SIGNUP_FETCH = "SIGNUP_FETCH";
export const HOME_FETCH = "HOME_FETCH";
export const PROFILE_FETCH ="PROFILE_FETCH";
export const CHOOSEE_FETCH = "CHOOSEE_FETCH";
export const COURSE_FETCH = "COURSE_FETCH";
